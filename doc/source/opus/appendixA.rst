.. _opus_appendixA:

Appendix A
==========

To make the DC1751 acquisition board compatible with the Xilinx dev-kit we had to perform some hardware modification to the latter.
This section presents the list of changes that have been made on the ZCU106 board **9TIC-1351**. 

Broken JTAG chain
-----------------

The ZCU106 has an automatic mechanism that allows to open the JTAG chain and to pass it through the FMC connector when a mezzanine board is present on the latter.
Unfortunately the DC1751 is not designed to handle the JTAG chain and therefore is unable to close it. 
This implies that when the DC1751 is coupled to the ZCU106 the JTAG chain remains open and becomes unusable. 
As a direct consequence the FPGA can no longer be configured via Vitis or Vivado. 
To fix this problem we had to remove from the ZCU106 the switch (**U27**) which is responsible for opening the JTAG chain and making a soldering bridge between the signals 
FPGA_TDO_HPC0_TDI_LS (pin 1) and FMC_HPC0_TDO_HPC1_TDI (pin 2).

Power Good
----------

A systematic check of all the pins of the FMC connector allowed us to discover that a voltage of 3.3V was applied to the F1 location of the connector on the dev-kit side. 
While on the DC1751 side the voltage of 1.8V is applied on the same pin. In order to fix this issue we decided to remove the resistor (**R277**) on the ZCU106 in order to 
prevent the board to apply its 3.3V on F1. This modification will not affect the correct behavior of the ZCU106 since F1 is not used inside the Xilinx dev-kit hardware.

