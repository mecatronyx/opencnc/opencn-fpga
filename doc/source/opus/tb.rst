.. _opus_tb:

Test bench
==========

The FPGA architecture of the OPUS project can be divided into three different groups. 
A software part that runs on the ARM processor of the FPGA, a transport and storage part based on the Xilinx IPs and the AXI network 
and a parallel computing part written by the REDS team in an HDL language.
The code executed on the ARM will be tested with specific software debugging tools that do not fall within the framework of this document, 
while the Xilinx IPs and the AXI network do not need any additional tests since they are fully validated by Xilinx. The test bench presented in 
this section will therefore concern the HDL part created during this project.

Test bench architecture 
-----------------------

An overview of the test bench structure developed to test the FPGA processing port is shown in :numref:`fig__opus_tb`.

.. figure:: figures/opus_tb.png
   :align: center
   :width: 600px
   :name: fig__opus_tb

   OPUS test bench overview.

The structure of the test bench depicted in :numref:`fig__opus_tb` can be divided into 3 parts. 
An input part, the DUT (device under test) which contains the HDL code that will be implemented in the FPGA 
and a recording block.

The input block
~~~~~~~~~~~~~~~~

To obtain a good flexibility the stimulus generator has been implemented in the following way. 
A file containing a signal generated with MATLab is read sequentially by a "reader" entity that 
formats the data read from files in samples designated to the DUT.
With this architecture the stimuli sent to the DUT are consistent with the MATLab scripts. 
Furthermore, changing the MATLab files allow the creation of new test-cases.

The DUT
~~~~~~~

As shown in :numref:`fig__opus_tb`, the DUT is composed of the DDC decimation block and the processing block presented :numref:`fig__opus_processing`.

Output recorder
~~~~~~~~~~~~~~~

Usually the output of a test bench contains a validation block capable of validate the DUT behavior on runtime. In this specific case the behavior 
of the processing block is still in progress, for this reason the test bench output is directly connected to a recording block that store all data to file. 
The latter will later be examined thanks to MATLab scripts.

TB utilization
--------------

The procedure to execute any modulator test bench is summarized below.

Create (if not present) a *comp* directory inside the *dev* directory of the project and enter into *comp*.

.. code-block:: bash

   > cd dev
   > mdir comp
   > cd comp

Lunch *vsim* with -do flag and the script associated with the desired test bench. 

.. code-block:: bash

   > $VSIM_PATH/vsim -do ../scripts/<script_name>.do

At this point Modelsim GUI will start executing the desired TB.


