.. _opus_architecture:

Hardware Architecture
=====================

This section describes the hardware setup which will be used in the framework of the OPUS project. 
The first part of this section presents the hardware choices made at the beginning of the project 
while the second part describes the FPGA architecture that will be developed in the first work packages.

Hardware setup
--------------

As already mentioned, the main goal of this project is to add to the OpenCN framework the possibility of acquiring analog signals 
in real time, preprocessing them inside an FPGA architecture and send the results to a machine regulation software running on an ARM CPU. 
The hardware setup used in the project must therefore consist of an analog signal acquisition part and an FPGA SoC system.
Given the short duration of the project and the budget available, a first implementation of the OPUS system will be deployed on hardware components available on the market.
The following specifications were used as inputs in order to choose the best components for our system:

Analog inputs 
   * Vibratory signals with a bandwidth of ~20kHz
   * Acoustic signals  with a bandwidth of 450kHz
   * Frequency after down scaling ~1.3kHz
   * ADC precision of 16bits

ARM specification 
   * Software shall be deployed on a quad-core ARMv8 (64-bit), for instance a Cortex-A7 -A53 or -A72.

Xilinx ZCU106
~~~~~~~~~~~~~

The ARM processor specifications were used to choose the FPGA SoC board needed to implement the OPUS architecture. 
Xilinx's ZCU106 Dev-kit was chosen for its versatility and for the fact that this board is already part of the hardware stock 
available to the REDS institute.
:numref:`fig__opus_zcu106` presents a picture of the FPGA based board.

.. figure:: figures/ZCU106.jpeg
   :align: center
   :width: 500px
   :name: fig__opus_zcu106

   Xilinx dev-kit ZCU106 

Although the ZCU106 has built-in ADC converters, they are not numerous enough to support the number of input channels required in the OPUS architecture. 
The decision was made to add an FMC mezzanine card dedicated for the acquisition of analog signals. This board is presented in the next section.


DC1751 acquisition board
~~~~~~~~~~~~~~~~~~~~~~~~

Finding an acquisition card capable of providing enough channels and compatible with the Low Pin Count (LPC) pin-out of the ZCU106's FMC connector was not an easy task.
Furthermore, the sampling rates required for this project (in MHz) is relatively low compared to current standards (several GSps).
We therefore had little choice for this part of our setup. The card that has been selected is the Linear evaluation board DC1751 (:numref:`fig__opus_dc1751`).

.. figure:: figures/DC1751.png
   :align: center
   :width: 300px
   :name: fig__opus_dc1751

   Linear evaluation board DC1751

The DC1751 is fully compatible with the LPC pin-out of the ZCU106 FMC connector (FMC2 in :numref:`fig__opus_zcu106`), the board converts up to
eight analog channel with a 16bits precision. The sampling rate is variable from 5MSps up to 125MSps. The digital outputs are serialized over 
standard LVDS lanes and will not required special treatments inside the FPGA design. 

To be noted that an external clock sources in needed in order to drive the board and therefore to define the actual sampling rate. 
A simple SPI interface is implement over the FMC connector and is used to configure the ADC chip. 

During the early stages of the project we ran into some compatibility problems between the DC1751 acquisition board and the Xilinx dev-kit.
To fix these problems we were forced to physically modify the Xilinx hardware. The list of the modifications can be found in :ref:`opus_appendixA`.

FPGA design
-----------

The first FPGA implementation presented in :numref:`fig__opus_top` aims to create a base structure centered on the AMR processor of the FPGA, in this first 
implementation stge the architecture shall provide an SPI block used to configure the DC1751 acquisition board.

.. figure:: figures/opus_top.png
   :align: center
   :width: 600px
   :name: fig__opus_top

   FPGA implementation for DC1751 bring-up

The block diagram depicted in :numref:`fig__opus_top` is composed of several elements:

   * **zynq_ultra_ps_e_0** : this block contains the ARM CPU
   * **rst_ps8_0_99M** : the element in charge of dispatching the reset inside the programmable logic (PL)
   * **ps8_0_axi_periph** : the axi switch
   * **axi_bram_ctrl_0** : used to bridge the AXI bus with an internal BRAM
   * **axi_bram_ctrl_bram** : the actual BRAM. This block will be replaced in future with an acquisition IP developed to receive data from the ADC hardware
   * **axi_quad_spi_0** : the SPI block used to configure the DC1751
   * **system_ila_o** : a special block used to record signals inside the PL and display them for debug purposes
   * **axi_gpio_0** : this block is used to access some leds on the ZCU106

A very simple baremetal application was developed at this stage of the development to configure the ADC component.

.. code-block:: c

    #include <stdio.h>
    #include "platform.h"
    #include "xil_printf.h"
    #include "xparameters.h"
    #include "xil_io.h"

    //current AXI resources mapping
    #define GPIO_ADDR 0xA0000000
    #define BRAM_ADDR 0xA0010000
    #define SPI_ADDR  0xA0020000

   int main()
   {
     init_platform();

     //SPI control register @60
     Xil_Out32(SPI_ADDR+0x60, 0x00000004);
     //LSB First                  = 0 = MSB first transfer format
     //Master Transaction Inhibit = 0 = Master transactions enabled
     //Manual Slave Select        = 0 = Slave select output asserted by master core logic
     //RX FIFO Reset              = 0
     //TX FIFO Reset              = 0
     //CPHA                       = 0 = ??
     //CPOL                       = 0 = Active-High clock; SCK idles Low
     //Master                     = 1
     //SPE                        = 0 = SPI system enabled
     //LOOP                       = 0 = Normal operation

     //SPI Slave Select @70
     Xil_Out32(SPI_ADDR+0x70, 0x00000001);

     //SPI DTR 68h
     Xil_Out32(SPI_ADDR+0x68, 0x0000005A);
     // test parttern to track on CS 
 
     //SPI control register @60 - ena ble transaction
     xil_printf("Enable SPI wrtie of  A5\r\n");
     Xil_Out32(SPI_ADDR+0x60, 0x00000 006);
     //LSB First                  = 0  = MSB first transfer format
     //Master Transaction Inhibit = 0  = Master transactions enabled
     //Manual Slave Select        = 0  = Slave select output asserted by master core logic
     //RX FIFO Reset              = 0 
     //TX FIFO Reset              = 0 
     //CPHA                       = 0  = ??
     //CPOL                       = 0  = Active-High clock; SCK idles Low
     //Master                     = 1
     //SPE                        = 1 = SPI system enabled
     //LOOP                       = 0 = Normal operation

    
     cleanup_platform();
     return 0;
   }

ADC SPI configuration
~~~~~~~~~~~~~~~~~~~~~

As presented in the previous section of code, the SPI interface for configuring the ADC module is mapped to the address 0xA0020000 on the AXI bus. 
The embedded FPGA CPU can therefore access the configuration registers of the LTM9011-14 thanks to the read and write commands (Xil_In32 and Xil_Out32) on the AXI interface.
The converter posses five 8-bits configuration registers:

 * **A0** : This register is composed of a single significant bit. D7 is used as software rest.
 * **A1** : A1 can be applied on channels 1,4,5 and 8 or on channels 2,3,6 and 7 depending on the CSA/CSB status. This register realize four operations: D7 (Clock Duty Cycle Stabilizer), D6 (Data Output Randomizer Mode), D5 (Two’s Complement Mode) and D4-0 are used to put channel in seep mode. 
 * **A2** : This register sets the output mode of the ADC pins. D7-5 select LVDS output driver current, B4 (LVDS Internal Termination), B3 enable/disable the output, B2-0 determine the type of serialization (number of lanes per samples and its granularity). 
 * **A3** : Test pattern register. D7 enable the test pattern generation. D5-0 set the test pattern for data bit 13 through data bit 8.
 * **A4** : D7-0 set the test pattern for data bit 7 through data bit 0.

For more information please refer to the Analog product data sheet.

Processing and Recording blocks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As already explained in section :ref:`opus_intro`, the FPGA design developed in the framework of the OPUS project 
must be able to record the RAW data stream coming from the acquisition board and in parallel it must 
be able to apply a set of computational processes on each channel. :numref:`fig__opus_rec_and_comp` shows the 
implemented architecture.

.. figure:: figures/opus_rec_and_comp.png
   :align: center
   :width: 600px
   :name: fig__opus_rec_and_comp

   FPGA implementation for Processing and Recording blocks

In :numref:`fig__opus_rec_and_comp`, two distinct clock domains are shown. A part of the blocks of the design belongs to the slow 
clock domain (roughly 6MHz) which is provided by the ADC board, other blocks works in the FPGA clock domain (200MHz clock) while the remaining 
blocks deal with the data transmission between the two domains of the clock.

ADC clock domain
   * SIPO : serial parallel interface. This block is responsible of recording serial inputs from the ADC and to recreate a N-bit samples (N is calculated according with the ADC granularity setting). SIPO can also be disabled by the Zynq in order to reduce de amount of data the FPGA will need to process (i.e. channel deactivation).
   * DDC : each channel input is connected to a DDC block which is in charge of decimate the input samples. The decimation value is programmable via the AXI CTRL block. 

Mix clock domain
   * Recording block : this block collects the data coming from the eight DDCs and store each sample in the BRAM. Moreover, the recording block associate a tag with the channel number to each sample recorded. 
   * Processing block : in parallel of being recorded by the recording block, each channel posses a processing block, the behavior of the latter is described in detail in the rest of this section.

FPGA clock domain
   * BRAM : Xilinx native BRAM element which can be natively mapped on the AXI bus.
   * AXI CTRL : this block is composed of a set of registers. These registers allow to activate/deactivate channels, to program DDC decimation value and FFT overlapping steps.
   
.. figure:: figures/opus_processing.png
   :align: center
   :width: 600px
   :name: fig__opus_processing

   The processing block in details

As already mentioned above, each input channel has its own processing block, the latter, in addition to dealing with the clock domain crossing signals, must perform a set of computational task as shown in :numref:`fig__opus_processing`.
For each single channel the work done by the processing block can be summarized in the following way.
The DDC incoming data are counted and stored in a dual port clock BRAM ensuring clock domain crossing in a safe and clean way. When the RAM has accumulated a sufficient number of data (i.e. 16384 for a 16k FFT) a start signal is sent into the FPGA clock domain. This signal triggers an FSM which
reads the BRAM and starts the FFT computation. When the FFT is done the FSM move its BRAM address pointer by a fixed value (overlap factor), controls if the BRAM contains enough new samples and it starts the next FFT. 
The output of the FFT is then sent to an additional processing element which calculates (in the current implementation) the square root of each sample of the FTT output vector. The result is then stored in a BRAM mapped on the AXI bus in order to make the data accessible from the Zynq.

Unfortunately, the design depicted in :numref:`fig__opus_rec_and_comp` will need too much FPGA resources for an eight channels implementation. With the deployment of new functionality and a lager FFT, a completely parallel 
architecture is impossible to realize within the FPGA chosen for the project. This led to a new architecture that allows to share the FFT block (the greediest in terms of memory resources) between different channels. 
This new architecture is presented in :numref:`fig__opus_rec_and_comp_final`.

.. figure:: figures/opus_rec_and_comp_final.png
   :align: center
   :width: 600px
   :name: fig__opus_rec_and_comp_final

   New FPGA implementation for Processing and Recording blocks. With a new functionality allowing FFT time sharing between multiple channels.

In the architecture presented in :numref:`fig__opus_rec_and_comp_final` the raw recording block has not undergone any changes while the processing block has been divided in two identical entities. Each part deals with four 
input channels. The new processing block includes a 16k-point FFT and input/output routing elements. An FSM has been inserted to manage the scheduling of the FFT sharing between the various channels. 
This FSM is in charge to ensure the calculation of one FFT per channel using a Round Robin strategy. This new type of architecture has made it possible to drastically reduce the use of FPGA resources 
(especially the BRAM blocks) and thus to be able to create a bitstream capable of managing eight channels simultaneously.

DC1751 interface specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section presents the way in which the signal samples generated by ADC board are transmitted to the FPGA pins and how the latter is configured to record and process the incoming data.

The LTM9011-14 ADC converter produces eight data channel with serialized one or two lane (configurable via SPI). The granularity of the samples can be configured in: 16, 14 or 12 bits.
In the 16 bits version, two zero are added in the lower part of the samples (value multiplied by 4), since the maximum granularity of the ADC is 14 bits. The 14 bits samples are composed 
of all the significant values produced by the ADC, while in the 14 bits version the two less significant bits are removed (actual value divided by 4). :numref:`fig__opus_ltm` shows the communication 
protocol of a 1 and 2 lanes serialization of a single channel with a sample granularity of 14 bits.

.. figure:: figures/opus_ltm.png
   :align: center
   :width: 600px
   :name: fig__opus_ltm

   LTM9011-14 one channel protocol (source : LTM9011-14 data sheet)


In the image shown in :numref:`fig__opus_ltm` everything but analog and ENC signals is an FPGA differential input. 

The signification of these signals is described in the list below:
   * analog : input signal to be converted by the ADC.
   * ENC  : sampling clock used by the ADC to sample the analog input. 
   * DCO  : this signal can be used a the clock to sample the incoming data.
   * FR   : the sample boundary are defined by the rising edge of this signal.
   * OUTA : incoming data for one or two lines configurations
   * OUTB : incoming data, only enables in two lines configuration. 

According to the diagram presented in :numref:`fig__opus_rec_and_comp`, each ser/par block have to handle DCO, FR, OUTA/B in order to record the incoming samples. 
Using DCO like a sampling clock, FR to start the parallelization task on OUTA/B inputs. The physical location of the eight channels is described in a Vivado 
configuration file (XDC). 
The XDC pins placement is dictated by the physical routing between the FMC connector on the Xilinx DevKit and the FPGA. 
Unfortunately, the DCO signals are directly routed on FPGA's non clock capable pins. This constraint prevent Vivado to use DCO to clock the FPGA internal logic. 
After a few attempts we decided to use the FR signal (the only one routed on a clock capable pin) to drive a PLL. The latter will multiply the FR frequency, according 
to the one or two line configuration and with the samples granularity, in order to obtain a clock signal synchronous with DCO. 
This work around looks solid enough to capture samples from the ADC for our POC, but the correct clock shall be used in a future version of the system. 

Complete architecture
~~~~~~~~~~~~~~~~~~~~~

All the blocks presented in this section are reachable by the FPGA ARM processor thanks to memory address mapped on the AXI bus. In this way the ARM can configure the 
different modules and / or retrieve the data produced by them. An overview of the full architecture is depicted in :numref:`fig__opus_full`.

.. figure:: figures/opus_full.png
   :align: center
   :width: 400px
   :name: fig__opus_full

   Opus final architecture overview.

Each element presented in :numref:`fig__opus_full` can be reached by the ARM processor through the memory map shown in the following table.

+------------+-------------------+
| Addr       | Description       |
+============+===================+
| A000_0000  | GPIO              |
+------------+-------------------+
| A001_0000  | Quad SPI          |
+------------+-------------------+
| A002_0000  | Recorder Control  |
+------------+-------------------+
| A003_0000  | Recorder Data     |
+------------+-------------------+
| A003_8000  | FFT- Channel 0    |
+------------+-------------------+
| A004_0000  | FFT- Channel 1    |
+------------+-------------------+
| A004_8000  | FFT- Channel 2    |
+------------+-------------------+
| A005_0000  | FFT- Channel 3    |
+------------+-------------------+
| A005_8000  | FFT- Channel 4    |
+------------+-------------------+
| A006_0000  | FFT- Channel 5    |
+------------+-------------------+
| A006_8000  | FFT- Channel 6    |
+------------+-------------------+
| A007_0000  | FFT- Channel 7    |
+------------+-------------------+

Address above A003_0000 are used to retrieve data only while A000_0000, A001_0000 and A002_0000 are targeting control interfaces. GPIO address is only used to turn on some leds, Quad SPI drive the SPI 
interface from the DC1751, its behavior has already been described previously in this document. A002_0000 is connected to the recorder control interface, the latter exposes a set of configuration registers
depicted in the next table. 

+------------+---------------------------------------+
| Offset     | Description                           |
+============+=======================================+
| 0000       | Enable raw (1 bit par active channel) |
+------------+---------------------------------------+
| 0004       | Channel 0 downsampler                 |
+------------+---------------------------------------+
| 0008       | Channel 1 downsampler                 |
+------------+---------------------------------------+
| 000C       | Channel 2 downsampler                 |
+------------+---------------------------------------+
| 0010       | Channel 3 downsampler                 |
+------------+---------------------------------------+
| 0014       | Channel 4 downsampler                 |
+------------+---------------------------------------+
| 0018       | Channel 5 downsampler                 |
+------------+---------------------------------------+
| 001C       | Channel 6 downsampler                 |
+------------+---------------------------------------+
| 0020       | Channel 7 downsampler                 |
+------------+---------------------------------------+

Here is a little description about the behavior of those registers.

  * Enable RAW :
    This register is composed of 8 useful bits in order to enable each channel in an independant way. You can use several channel at a time. As exemple, if you want to record raw data for channel 0 and 3, set this register value to 0b00001001 (0x09). 
  * Downsampler :
    This value define the downsampling factor inside each Ser/Par component. Since the AD converter does not support a sampling frequency below 10MHz, we have to downsample the date flow in order to keep FFT as accurate as possible. At this point, we must acquire input dataflow at 2MHz. To achieve this constraint, set those eight registers to 0x05. 
