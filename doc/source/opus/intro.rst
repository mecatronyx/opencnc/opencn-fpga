.. _opus_intro:   

Introduction
============

This document describes the work performed by the REDS team in the framework of the OPUS project. 
The aim of this project is to develop, in collaboration with .... TBC.
The OPUS project started in September 2021 and ended in XXX 2021.
This document presents the work done during these months.

General Idea
------------

The outline of the entire architecture of the OPUS system is shown in :numref:`fig__opus_full_arch`.

.. figure:: figures/top_design.jpeg
   :align: center
   :width: 500px
   :name: fig__opus_full_arch

   General idea of the OPUS system 

The FPGA architecture depicted in :numref:`fig__opus_full_arch` will be designed to provide the Zynq with three distinct functionalities.

* AXI to SPI : allows the correct correct configuration of the acquisition board via an memory mapped AXI interface.
* Recoding block : a recording block will be implemented in order to acquire the RAW data flow from the eight channels. Initially, the recorded samples will be accessible from the Zynq via a BRAM memory mapped on the AXI bus.
* Processing block : in parallel with the recording block each data channel will be connected with a processing block. The latter will execute a pre defined signal processing (the same for each channel). The output of each processing block will be available on AXI memory mapped BRAMs.

Section :ref:`opus_architecture` presents in details the architecture of the Recording and Processing blocks. 
