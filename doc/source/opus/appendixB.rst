.. _opus_appendixB:

Appendix B
==========

This section provides all the information necessary to resume the Vivado project created within the framework of 
this project and to create an FPGA programming file known as XSA file. 

Vivado project
--------------

This project was created with Vivado version 2020.2. After a few attempts to add the project on a GIT repository we realized that 
the file structure of a Vivado project does not fit very well the GIT methodology. In fact, the result of this process did not seem satisfactory to us.
We therefore chose to store our Vivado project as a compressed archive when finished. This allows to easily retrieve all the project content as long as 
the same version of Vivado is used to open the project. 

XSA creation
------------

Once the Vivado project has been successfully opened, the steps to perform to generate an XSA file are as follows.


First step is to lunch the code synthesis and the place & route task by clicking the generate bitstream button on the bottom
of the flow navigator window (see :numref:`fig__gen_bitstream`). This process can take several minutes, even longer than one hour 
on a slow computer.  

.. figure:: figures/gen_bitstream.png
   :align: center
   :width: 200px
   :name: fig__gen_bitstream

   Generate the bitstream to obtain a configuration file.

Once the bistream generation done go to **File -> Export -> Export Hardware**, the following window will appear (:numref:`fig__export_hw`). Click Next.

.. figure:: figures/export_hw.png
   :align: center
   :width: 400px
   :name: fig__export_hw

   Export hardware window.

A second window will pop-up (:numref:`fig__export_hw2`). Check the "include bitstream" option and click Next.

.. figure:: figures/export_hw2.png
   :align: center
   :width: 400px
   :name: fig__export_hw2

   Export hardware window (cnt).

:numref:`fig__export_hw3` shows the last step of the creation process. Choose a file name and a directory to store the XSA, then click Next (or Finish) to start the file creation. 

.. figure:: figures/export_hw3.png
   :align: center
   :width: 400px
   :name: fig__export_hw3

   Export hardware window, last step.