#!/bin/bash

# -- This script generates OpenCN documentations inside a docker container.


SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

docker run --rm -it -v $SCRIPTPATH/..:/home/reds/opencn opencn-fpga /home/reds/opencn/doc/generate_doc.sh $@
